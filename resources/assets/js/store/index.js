import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    nombre:"danieldelgado",
    counter:0,
    user: null,
    roles: []
  },
  mutations: {
      setAuthUser(state, user) {
        state.user = user;
      },
      setRolesUser(state, roles) {
        state.roles = roles;
      }
  },
  actions: {
  },
  modules: {
  },
  getters: {
    isLoggedIn(state) {
        return state.user != null;
    },
    getUser(state){
      return state.user;
    },
    getRoles(state) {
      if(state.roles)
        return state.roles;
      else
        return false;
    }
  }
});
