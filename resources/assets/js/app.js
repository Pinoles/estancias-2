
require('./bootstrap');

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Axios from 'axios'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
 
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false;

Axios.baseURL = 'http://127.0.0.1:8000';
Axios.timeout = 2000;

Vue.prototype.$Axios = Axios;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
