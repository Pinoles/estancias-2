import FoodsIndex from '../views/Foods/FoodsIndex.vue'
import FoodsCreate from '../views/Foods/FoodsCreate.vue'
import FoodsShow from '../views/Foods/Foodshow.vue'
import FoodEdit from '../views/Foods/FoodEdit.vue'

const FoodIndex = [
    {
        path:'',
        name:'Foods',
        component: FoodsIndex
      },
      {
        path:'create',
        name:'FoodsCreate',
        component: FoodsCreate
      },
      {
        path: ':FoodId',
        name: 'FoodsShow',
        component: FoodsShow
      },
      {
        path: ':FoodId/edit',
        name: 'FoodsEdit',
        component: FoodEdit
      }
];

export default FoodIndex;