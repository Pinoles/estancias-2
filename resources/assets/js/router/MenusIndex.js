import MenusIndex from '../views/Menus/MenusIndex.vue'
import MenusCreate from '../views/Menus/MenusCreate.vue'
import MenusShow from '../views/Menus/Menushow.vue'
import MenuEdit from '../views/Menus/MenuEdit.vue'
import MenuDisplay from '../views/Menus/MenuDisplay.vue'

const MenuIndex = [
    {
        path:'',
        name:'Menus',
        component: MenusIndex
      },
      {
        path:'create',
        name:'MenusCreate',
        component: MenusCreate
      },
      {
        path: ':menuId',
        name: 'MenusShow',
        component: MenusShow
      },
      {
        path: ':menuId/edit',
        name: 'MenusEdit',
        component: MenuEdit
      },
      {
        path: ':menuId/display',
        name: 'MenuDisplay',
        component: MenuDisplay
      }
];

export default MenuIndex;