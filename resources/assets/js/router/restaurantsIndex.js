import Restaurants from '../views/Restaurants/Restaurants.vue'
import RestaurantsCreate from '../views/Restaurants/RestaurantsCreate.vue'
import RestaurantsShow from '../views/Restaurants/RestaurantShow.vue'
import RestaurantEdit from '../views/Restaurants/RestaurantEdit.vue'
import MenusRoute from '../views/Menus/MenusRoute.vue'
import MenuIndex from './MenusIndex.js'
import FoodsRoute from '../views/Foods/FoodRoute.vue'
import FoodIndex from './FoodsIndex.js'
import OrdersRoute from '../views/Orders/OrdersRoute.vue'
import OrderIndex from './OrdersIndex.js'

const RestaurantIndex = [
    {
        path:'',
        name:'Restaurants',
        component: Restaurants
      },
      {
        path:'create',
        name:'RestaurantsCreate',
        component: RestaurantsCreate
      },
      {
        path: ':resId',
        component: RestaurantsShow,
        children:[
          {
            path: '',
            name: 'RestaurantsShow',
            component: null
          },
          {
            path:'Menus',
            component: MenusRoute,
            children: MenuIndex
          },
          {
            path: 'Foods',
            component: FoodsRoute,
            children: FoodIndex
          },
          {
            path: 'Orders',
            component: OrdersRoute,
            children: OrderIndex
          }
        ]
      },
      {
        path: ':resId/edit',
        name: 'RestaurantsEdit',
        component: RestaurantEdit
      }
];

export default RestaurantIndex;