import OrdersIndex from '../views/Orders/OrdersIndex.vue'
import OrdersCreate from '../views/Orders/OrdersCreate.vue'
import OrdersShow from '../views/Orders/Ordershow.vue'
import OrderEdit from '../views/Orders/OrderEdit.vue'

const OrderIndex = [
    {
        path:'',
        name:'Orders',
        component: OrdersIndex
      },
      {
        path:'create',
        name:'OrdersCreate',
        component: OrdersCreate
      },
      {
        path: ':OrderId',
        name: 'OrdersShow',
        component: OrdersShow
      },
      {
        path: ':OrderId/edit',
        name: 'OrdersEdit',
        component: OrderEdit
      }
];

export default OrderIndex;