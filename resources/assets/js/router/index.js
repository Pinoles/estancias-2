import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import RestaurantRoute from '../views/Restaurants/RestaurantRoute.vue'
import RestaurantIndex from './restaurantsIndex.js'
import ConsumirApi from '../views/ConsumirApi.vue'

//hardcored code
import RestaurantsClient from '../views/Restaurants/RestaurantsClient'
import MenuClient from '../views/Menus/MenuClient.vue'
import OrderClient from '../views/Orders/OrderClient.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/restaurants',
    component: RestaurantRoute,
    children: RestaurantIndex,
    meta: {
      requiresAuth: true,
      requiresOwner: true
    },
  },
  {
    path: '/client',
    name: 'Client',
    component: RestaurantsClient,
    meta:{
      requiresAuth:true
    }
  },
  {
    path:'/client/:resId',
    name:'MenuDisplayClient',
    component: MenuClient,
    meta:{
      requiresAuth:true
    }
  },
  {
    path: '/client/:resId/order',
    name:'OrderClient',
    component: OrderClient,
    meta:{
      requiresAuth:true
    }
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login
  },
  {
    path:'/api',
    name:'ConsumirApi',
    component: ConsumirApi
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requiresAuth)) {
    if (store.state.user || window.auth_user) {
      if(to.matched.some(route => route.meta.requiresOwner)){
        if(store.state.roles.includes('Owner') || window.roles.includes('Owner')){
          next();
        }else{
          next({path: '/home'});
        }
      }else{
        next();
      }
    } else {
      next({ path: '/login' });
    }
  }else{
    if (to.name == 'Login' && (store.state.user || window.auth_user)) {
      next({ path: '/home' });
    } else {
      next();
    }
  }
});

export default router
