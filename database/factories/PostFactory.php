<?php

use Faker\Generator as Faker;

$factory->define(App\restaurants::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->company,
        'telephone' => $faker->phoneNumber,
        'restaurant_contact' => $faker->name,
        'cp' => 12000,
        'estado' => $faker->state,
        'municipio' => $faker->city,
        'colonia' => $faker->streetName,
        'calle' => $faker->streetAddress,
        'user_id' => $faker->numberBetween($min = 1, $max = 20)
    ];
});
