<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\User::class,20)->create();

        Role::create([
            'name' => 'Admin'
        ]);

        Role::create([
            'name' => 'Owner'
        ]);

        Role::create([
            'name' => 'Client'
        ]);
    }
}
