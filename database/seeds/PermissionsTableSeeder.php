<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Permission::create([
            'name' => 'Manejo de usuarios'
        ]);

        Permission::create([
            'name' => 'Manejo de restaurante'
        ]);

        Permission::create([
            'name' => 'Manejo de Menu'
        ]);
    }
}
