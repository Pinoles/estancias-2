<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodsMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foods_menus', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedInteger('foods_id');
            $table->foreign('foods_id')
                ->references('id')
                ->on('foods')->onDelete('cascade');

            $table->unsignedInteger('menus_id');
            $table->foreign('menus_id')
                ->references('id')
                ->on('menus')->onDelete('cascade');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foods_menus');
    }
}
