<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_foods', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('orders_id');
            $table->foreign('orders_id')
                ->references('id')
                ->on('orders')->onDelete('cascade');

            $table->unsignedInteger('foods_id');
            $table->foreign('foods_id')
                ->references('id')
                ->on('foods')->onDelete('cascade');

            $table->integer('food_quantity');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_foods');
    }
}
