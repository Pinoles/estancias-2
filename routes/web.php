<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$view = View::make('home', [
    'auth_user' => Auth()->user()
]);

Route::get('/login', function () {
    return view("home",[
        'auth_user' => Auth()->user()
    ]);
})->name('frontlogin');



//Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::prefix('/wapi')->group(function () {
    Auth::routes();
    
    Route::middleware('auth')->group(function () {
        Route::get('/user/restaurants','UserController@getRestaurants');

        Route::get('/restaurants/{restaurant}/menus','RestaurantsController@getMenus');
        Route::get('/restaurants/{restaurant}/foods','RestaurantsController@getFoods');
        Route::get('/restaurants/{restaurant}/orders','RestaurantsController@getOrders');
        Route::get('/restaurants/{restaurant}/activeMenu','RestaurantsController@activeMenu');
        Route::resource('/restaurants','RestaurantsController');

        Route::get('/menus/{idmenu}/foods', 'MenusController@getFoods');
        Route::post('/menus/{idmenu}/attach', 'MenusController@attachFood');
        Route::get('/menus/{idmenu}/display','MenusController@display');
        Route::resource('/menus','MenusController');

        Route::get('/foods/{idmenu}/menus', 'FoodsController@getMenus');
        Route::resource('/foods','FoodsController');

        Route::get('/orders/{order}/foods','OrdersController@getFoods');
        Route::post('/orders/{order}/attach','OrdersController@attachFood');
        Route::post('/orders/attachClient', 'OrdersController@attachFoodClient');
        Route::resource('/orders','OrdersController');
    });
});

Route::middleware('auth')->group(function () {

    Route::group(['middleware' => ['role:Client']], function () {
        //
        $auth_user = [
            'auth_user' => Auth()->user()
        ];
        
        Route::view('/client','home',$auth_user );
        Route::view('/client/{restaurant}','home',$auth_user );
        Route::view('/client/{restaurant}/order','home',$auth_user);
    });

    Route::group(['middleware' => ['role:Owner']], function () {
        //
        Route::prefix('/restaurants')->group(function () {
            $auth_user = [
                'auth_user' => Auth()->user()
            ];
    
            Route::view('/','home',$auth_user);
            Route::view('/create', 'home',$auth_user);
            Route::view('/{id}','home',$auth_user);
            Route::view('/{id}/edit','home',$auth_user);
            Route::view('/{id}/create','home',$auth_user);
        
            Route::prefix('/{id}/Menus')->group(function (){
                $auth_user = [
                    'auth_user' => Auth()->user()
                ];
                Route::view('/','home',$auth_user);
                Route::view('/create', 'home',$auth_user);
                Route::view('/{idmenu}','home',$auth_user);
                Route::view('/{idmenu}/edit','home',$auth_user);
                Route::view('/{idmenu}/create','home',$auth_user);
                Route::view('/{idmenu}/display','home',$auth_user);
            });
        
            Route::prefix('/{id}/Foods')->group(function (){
                $auth_user = [
                    'auth_user' => Auth()->user()
                ];
                Route::view('/','home',$auth_user);
                Route::view('/create', 'home',$auth_user);
                Route::view('/{idfood}','home',$auth_user);
                Route::view('/{idfood}/edit','home',$auth_user);
                Route::view('/{idfood}/create','home',$auth_user);
            });
    
            Route::prefix('/{id}/Orders')->group(function (){
                $auth_user = [
                    'auth_user' => Auth()->user()
                ];
                Route::view('/','home',$auth_user);
                Route::view('/create', 'home',$auth_user);
                Route::view('/{idorder}','home',$auth_user);
                Route::view('/{idorder}/edit','home',$auth_user);
                Route::view('/{idorder}/create','home',$auth_user);
            });
        });
    });
});