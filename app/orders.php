<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class orders extends Model
{
    //
    protected $table='orders';

	use SoftDeletes;

	// Eloquent asume que cada tabla tiene una clave primaria con una columna llamada id.
	// Si éste no fuera el caso entonces hay que indicar cuál es nuestra clave primaria en la tabla:
	protected $primaryKey = 'id';

	// Atributos que se pueden asignar de manera masiva.
	protected $fillable = array('name','total_price','payed','restaurants_id');	
	// Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
	protected $hidden = ['created_at','updated_at', 'deleted_at','pivot']; 

	public function foods()
	{
		return $this->belongsToMany(
				'App\foods',
				'orders_foods',
				'orders_id',
				'foods_id')
				->withPivot('food_quantity');
	}
}
