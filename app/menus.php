<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class menus extends Model
{
    //
    protected $table='menus';

	use SoftDeletes;

	// Eloquent asume que cada tabla tiene una clave primaria con una columna llamada id.
	// Si éste no fuera el caso entonces hay que indicar cuál es nuestra clave primaria en la tabla:
	protected $primaryKey = 'id';

	// Atributos que se pueden asignar de manera masiva.
	protected $fillable = array('name','start_hour','end_hour','restaurant_id');
	
	// Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
	protected $hidden = ['created_at','updated_at', 'deleted_at','pivot']; 

	public function foods()
	{
		return $this->belongsToMany(
				'App\foods',
				'foods_menus',
				'menus_id',
				'foods_id');
	}
}
