<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class restaurants extends Model
{
    //
    protected $table='restaurants';

	use SoftDeletes;

	// Eloquent asume que cada tabla tiene una clave primaria con una columna llamada id.
	// Si éste no fuera el caso entonces hay que indicar cuál es nuestra clave primaria en la tabla:
	protected $primaryKey = 'id';

	// Atributos que se pueden asignar de manera masiva.
	protected $fillable = array('name','telephone','restaurant_contact','cp','estado','municipio','colonia','calle','user_id');
	
	// Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
	protected $hidden = ['created_at','updated_at']; 

	public function menus()
    {
        return $this->hasMany('App\menus','restaurant_id');
	}
	
	public function foods(){
		return $this->hasMany('App\foods');
	}
	
	public function orders(){
		return $this->hasMany('App\orders');
	}
}
