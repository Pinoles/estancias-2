<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\menus;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['status'=>'ok','data'=>menus::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'start_hour' => 'required',
            'end_hour' => 'required',
            'restaurant_id' => 'required'
        ]);

        menus::create([
            'name' => $request->name,
            'start_hour' => $request->start_hour,
            'end_hour' => $request->end_hour,
            'restaurant_id' => $request->restaurant_id
        ]);

        return response()->json(['status'=>'ok'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(['status'=>'ok','data'=>menus::find($id)], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return response()->json(['status'=>'ok','data'=>menus::find($id)], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required',
            'start_hour' => 'required',
            'end_hour' => 'required'
        ]);

        $menu = menus::find($id);
        
        $menu->name = $request->name;
        $menu->start_hour = $request->start_hour;
        $menu->end_hour = $request->end_hour;

        $menu->save();

        return response()->json(['status'=>'ok'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $id = menus::find( $id );
        $id ->delete();
    }

    public function display($id){
        $menu = menus::find($id);
        return response()->json(['status'=>'ok','data'=>['menu' => $menu, 'foods' => $menu->foods()->orderBy('name')->get()]], 200);
    }

    public function getFoods($id){
        $foods = menus::find($id)->foods;
        return response()->json(['status'=>'ok','data'=>$foods], 200);
    }

    public function attachFood(Request $request, $id){
        $menu = menus::find($id);
        $reqFoods = (array) $request->foods;
        $menu->foods()->sync($reqFoods);

        return response()->json(['status'=>'ok'], 200);
    }
}
