<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\restaurants;

class RestaurantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        //
        return response()->json(['status'=>'ok','data'=>restaurants::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = Auth()->user();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'telephone' => 'required',
            'restaurant_contact' => 'required',
            'cp' => 'required',
            'estado' => 'required',
            'municipio' => 'required',
            'colonia' => 'required',
            'calle' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>'fail', 'errors' => $validator->errors()], 400);
        }

        restaurants::create([
            'name' => $request->name,
            'telephone' => $request->telephone,
            'restaurant_contact' => $request->restaurant_contact,
            'cp' => $request->cp,
            'estado' => $request->estado,
            'municipio' => $request->municipio,
            'colonia' => $request->colonia,
            'calle' => $request->calle,
            'user_id' => $user->id
        ]);

        return response()->json(['status'=>'ok'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $menus = restaurants::find($id)->menus;
        return response()->json(['status'=>'ok','data'=>$menus], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return response()->json(['status'=>'ok','data'=>restaurants::find($id)], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required',
            'telephone' => 'required',
            'restaurant_contact' => 'required',
            'cp' => 'required',
            'estado' => 'required',
            'municipio' => 'required',
            'colonia' => 'required',
            'calle' => 'required'
        ]);

        $restaurant = restaurants::find($id);
        
        $restaurant->name = $request->name;
        $restaurant->telephone = $request->telephone;
        $restaurant->restaurant_contact = $request->restaurant_contact;
        $restaurant->cp = $request->cp;
        $restaurant->estado = $request-> estado;
        $restaurant->municipio = $request->municipio;
        $restaurant->colonia = $request->colonia;
        $restaurant->calle = $request->calle;

        $restaurant->save();

        return response()->json(['status'=>'ok'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $id = restaurants::find( $id );
        $id ->delete();
    }

    public function getMenus($id){
        //
        $menus = restaurants::find($id)->menus;
        return response()->json(['status'=>'ok','data'=>$menus], 200);
    }

    public function getFoods($id){
        //
        $foods = restaurants::find($id)->foods;
        return response()->json(['status'=>'ok','data'=>$foods], 200);
    }

    public function getOrders($id){
        //
        $Orders = restaurants::find($id)->orders;
        return response()->json(['status'=>'ok','data'=>$Orders], 200);
    }

    public function activeMenu($id){
        $menus = restaurants::find($id)->menus;
        $flag = FALSE;
        $menu = null;
        
        $now = \Carbon\Carbon::now();
        $currentTime = \Carbon\Carbon::createFromFormat('H:i:s',$now->toTimeString());
        foreach ($menus as $key => $value) {
            # code...

            $startTime =\Carbon\Carbon::createFromFormat('H:i:s',$value->start_hour);
            $endTime = \Carbon\Carbon::createFromFormat('H:i:s',$value->end_hour);
            if($currentTime->between($startTime, $endTime) && !$flag) {
               $menu = $value;
               $flag = TRUE;
            }
        }
        if($flag){
            return response()->json(['status'=>'ok','menuid'=>$menu->id], 200);
        }else{
            echo($currentTime);
            return response()->json(['status'=>'fail','errors'=>'nose encontro menu'], 404);
        }
    }
}
