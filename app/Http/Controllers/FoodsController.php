<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\foods;

class FoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['status'=>'ok','data'=>foods::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'weight' => 'required',
            'price' => 'required',
            'content' => 'required',
            'restaurants_id' => 'required'
        ]);

        foods::create([
            'name' => $request->name,
            'type' => $request->type,
            'weight' => $request->weight,
            'price' => $request->price,
            'content' => $request->content,
            'restaurants_id' => $request->restaurants_id
        ]);

        return response()->json(['status'=>'ok'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(['status'=>'ok','data'=>foods::find($id)], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return response()->json(['status'=>'ok','data'=>foods::find($id)], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'content' => 'required',
            'weight' => 'required',
            'price' => 'required'
        ]);

        $food = foods::find($id);
        
        $food->name = $request->name;
        $food->type = $request->type;
        $food->content = $request->content;
        $food->weight = $request->weight;
        $food->price = $request->price;

        $food->save();

        return response()->json(['status'=>'ok'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $id = foods::find( $id );
        $id ->delete();
    }

    public function getMenus($id){
        $menus = foods::find($id)->menus;
        return response()->json(['status'=>'ok','data'=>$menus], 200);
    }
}
