<?php

namespace App\Http\Controllers\Auth;

use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use \Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

   /**
 * Send the response after the user was authenticated.
 *
 * @param  \Illuminate\Http\Request $request
 * @return \Illuminate\Http\Response
 */
protected function sendLoginResponse(Request $request)
{
    $request->session()->regenerate();

    $this->clearLoginAttempts($request);
    if ($request->wantsJson()) {
        $user =  $this->guard()->user();
        return response()->json([
            'user' => $user,'roles' =>$user->getRoleNames()
        ]); 
    }

    $user =  $this->guard()->user();
    return response()->json([
        'user' => $user,'roles' =>$user->getRoleNames()
    ]); 

    //return $this->authenticated($request, $this->guard()->user())
      //  ?: redirect()->intended($this->redirectPath());
}

/**
 * Get the failed login response instance.
 *
 * @param \Illuminate\Http\Request $request
 * @return \Illuminate\Http\RedirectResponse
 */
protected function sendFailedLoginResponse(Request $request)
{
    if ($request->wantsJson()) {
        return response()->json([
            $this->username() => \Lang::get('auth.failed'),
        ], 422);
    }

    return redirect()->back()
        ->withInput($request->only($this->username(), 'remember'))
        ->withErrors([
            $this->username() => Lang::get('auth.failed'),
        ]);
}
}
