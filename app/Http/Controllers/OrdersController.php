<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\orders;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(['status'=>'ok','data'=>orders::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'restaurants_id' => 'required'
        ]);

        orders::create([
            'name' => $request->name,
            'restaurants_id' => $request->restaurants_id
        ]);

        return response()->json(['status'=>'ok'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(['status'=>'ok','data'=>orders::find($id)], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return response()->json(['status'=>'ok','data'=>orders::find($id)], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required',
            'total_price' => 'required',
            'payed' => 'required'
        ]);

        $order = orders::find($id);
        
        $order->name = $request->name;
        $order->total_price = $request->total_price;
        $order->payed = $request->payed;

        $order->save();

        return response()->json(['status'=>'ok'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $order = orders::find( $id );
        $order ->delete();
    }

    public function display($id){
        $order = orders::find($id);
        return response()->json(['status'=>'ok','data'=>['order' => $order, 'foods' => $order->foods()->orderBy('name')->get()]], 200);
    }

    public function getFoods($id){
        $foods = orders::find($id)->foods;
        return response()->json(['status'=>'ok','data'=>$foods], 200);
    }

    public function attachFood(Request $request, $id){
        $order = orders::find($id);
        
        $order->payed = $request->payed;        
        $order->total_price = $request->total_price;

        $reqFoods = (array) $request->foods;
        $reqQuantitys = (array) $request->quantitys;
        $syncFoods = array();

        for ($i=0; $i < count($reqFoods); $i++) { 
            $syncFoods[$reqFoods[$i]] = ['food_quantity' => $reqQuantitys[$i]];
        }
        $order->foods()->sync($syncFoods);

        $order->save();

        return response()->json(['status'=>'ok'], 200);
    }

    public function attachFoodClient(Request $request){

        $order = $request->Order;

        $order = orders::create([
            'name' => $order['name'],
            'payed' => $order['payed'],
            'total_price' => $order['total_price'],
            'restaurants_id' => $order['restaurants_id']
        ]);

        //$order->payed = $request->payed;        
        //$order->total_price = $request->total_price;

        $reqFoods = (array) $request->foods;
        $reqQuantitys = (array) $request->quantitys;
        $syncFoods = array();

        for ($i=0; $i < count($reqFoods); $i++) { 
            $syncFoods[$reqFoods[$i]] = ['food_quantity' => $reqQuantitys[$i]];
        }
        $order->foods()->sync($syncFoods);

        //$order->save();

        return response()->json(['status'=>'ok'], 200);
    }
}
