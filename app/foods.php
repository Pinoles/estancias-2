<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class foods extends Model
{
    //
    protected $table='foods';

	use SoftDeletes;

	// Eloquent asume que cada tabla tiene una clave primaria con una columna llamada id.
	// Si éste no fuera el caso entonces hay que indicar cuál es nuestra clave primaria en la tabla:
	protected $primaryKey = 'id';

	// Atributos que se pueden asignar de manera masiva.
	protected $fillable = array('name','type','content','weight','price','restaurants_id');
	
	// Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
	protected $hidden = ['created_at','updated_at', 'restaurants_id' , 'deleted_at']; 

	public function menus()
	{
		return $this->belongsToMany(
				'App\menus',
				'foods_menus',
				'foods_id',
				'menus_id');
	}
}
